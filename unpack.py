#!/usr/bin/env python

import sys, os, string
import struct
import zlib

from StringIO import StringIO

if len(sys.argv) < 3:
	script_file_name = os.path.split(sys.argv[0])[1]
	print 'usage: {0} <big file> <output directory>'.format(script_file_name)
	sys.exit()

input_file_path = sys.argv[1]
if not os.path.isfile(input_file_path):
	print 'error: invalid input file specified'
	sys.exit()

output_directory = sys.argv[2]
if os.path.exists(output_directory):
	if not os.path.isdir(output_directory):
		print 'error: invalid output directory specified'
		sys.exit()
else:
	os.makedirs(output_directory)

def check_file_magic(f, expected_magic):
	old_offset = f.tell()
	try:
		magic = f.read(len(expected_magic))
	except:
		return False
	finally:
		f.seek(old_offset)
	return magic == expected_magic

def read_cstring(f):
	bytes = []
	while True:
		byte = f.read(1)
		if byte == '\0':
			break
		elif byte == '':
			raise EOFError()
		else:
			bytes.append(byte)
	return ''.join(bytes)

def align_up(x, alignment):
	return (x + (alignment - 1)) & ~(alignment - 1)

def refunpack(data):
	bi = bytearray(); bo = bytearray()
	bi.extend(data)
	opos = ipos = 0

	signature = (bi[ipos + 0] << 8) | bi[ipos + 1]; ipos += 2
	if (signature & 0x0100) != 0:
		compressed_size = (bi[ipos + 0] << 16) | (bi[ipos + 1] << 8) | bi[ipos + 2]; ipos += 3
	else:
		compressed_size = 0
	uncompressed_size = (bi[ipos + 0] << 16) | (bi[ipos + 1] << 8) | bi[ipos + 2]; ipos += 3

	while True:
		b0 = bi[ipos]; ipos += 1
		if (b0 & 0x80) == 0: # 2-byte command: 0DDRRRPP DDDDDDDD
			b1 = bi[ipos]; ipos += 1
			count = b0 & 0x3
			for i in xrange(count):
				bo.append(bi[ipos]); ipos += 1;
				opos += 1
			rpos = opos - ((b0 & 0x60) << 3) - b1 - 1
			count = ((b0 >> 2) & 0x7) + 3
			for i in xrange(count):
				bo.append(bo[rpos]); rpos += 1
				opos += 1
		elif (b0 & 0x40) == 0: # 3-byte command: 10RRRRRR PPDDDDDD DDDDDDDD
			b1 = bi[ipos]; ipos += 1
			b2 = bi[ipos]; ipos += 1
			count = b1 >> 6
			for i in xrange(count):
				bo.append(bi[ipos]); ipos += 1;
				opos += 1
			rpos = opos - ((b1 & 0x3F) << 8) - b2 - 1
			count = (b0 & 0x3F) + 4
			for i in xrange(count):
				bo.append(bo[rpos]); rpos += 1
				opos += 1
		elif (b0 & 0x20) == 0: # 4-byte command: 110DRRPP DDDDDDDD DDDDDDDD RRRRRRRR
			b1 = bi[ipos]; ipos += 1
			b2 = bi[ipos]; ipos += 1
			b3 = bi[ipos]; ipos += 1
			count = b0 & 0x3
			for i in xrange(count):
				bo.append(bi[ipos]); ipos += 1;
				opos += 1
			rpos = opos - ((b0 & 0x10) << 12) - (b1 << 8) - b2 - 1
			count = ((b0 & 0xC) << 6) + b3 + 5
			for i in xrange(count):
				bo.append(bo[rpos]); rpos += 1
				opos += 1
		else: # 1-byte command: 111PPPPP
			count = (b0 & 0x1F) * 4 + 4
			if count <= 0x70:
				# no stop flag
				for i in xrange(count):
					bo.append(bi[ipos]); ipos += 1;
					opos += 1
			else:
				# stop flag
				count = b0 & 0x3
				for i in xrange(count):
					bo.append(bi[ipos]); ipos += 1;
					opos += 1
				break

	return str(bo)

BIG_MAGIC = '\x45\x42\x00\x03'

HEADER_FMT = '>4sIIII2BHII16x'

class Entry(object):
	FMT = '>IIIQ'

	def __init__(self):
		self.offset = None
		self.pad = None # always 0
		self.size = None
		self.unk_0x0C = None

		self.path = None

	def load(self, f):
		self.offset, self.pad, self.size, self.unk_0x0C = struct.unpack(Entry.FMT, f.read(struct.calcsize(Entry.FMT)))

		# offset is shifted
		self.offset *= 0x10

		if self.pad != 0:
			print 'error: odd padding'
			sys.exit()

	def dump(self):
		print 'offset:0x{0:08X} size:0x{1:08X} unk:0x{2:016X}'.format(self.offset, self.size, self.unk_0x0C)

class Chunk(object):
	FMT = '>II'

	def __init__(self):
		self.compressed_size = None
		self.flags = None # 0x01 - compressed, 0x02 - ???, 0x04 - not compressed?

		self.offset = None
		self.is_compressed = None

	def load(self, f):
		self.compressed_size, self.flags = struct.unpack(Chunk.FMT, f.read(struct.calcsize(Chunk.FMT)))

		if not self.flags in [0x01, 0x02, 0x04]:
			print 'error: flags: 0x{0:08X}'.format(self.flags)
			sys.exit()

		self.offset = f.tell()
		self.is_compressed = self.flags in [0x01, 0x02]

class SuperChunk(object):
	MAGIC_ZIP = 'chunkzip'
	MAGIC_REFPACK = 'chunkref'
	FMT = '>8sIIIII'

	def __init__(self):
		self.magic = None
		self.unk_0x04 = None # 0x02?
		self.uncompressed_size = None
		self.unk_0x10 = None # 0x20000?
		self.num_chunks = None
		self.unk_0x18 = None # 0x10?
		self.unk_0x1C = None # 0x00?

		self.chunks = None

	def load(self, f):
		if not check_file_magic(f, SuperChunk.MAGIC_ZIP) and not check_file_magic(f, SuperChunk.MAGIC_REFPACK):
			print 'error: invalid chunk descriptor format'
			sys.exit()

		self.magic, self.unk_0x04, self.uncompressed_size, self.unk_0x10, self.num_chunks, self.unk_0x18 = struct.unpack(SuperChunk.FMT, f.read(struct.calcsize(SuperChunk.FMT)))

		if self.uncompressed_size == 0:
			return

		self.unk_0x1C, = struct.unpack('>I', f.read(struct.calcsize('I')))

		if self.unk_0x04 != 0x02:
			print 'error: unk_0x04: 0x{0:08X}'.format(self.unk_0x04)
			sys.exit()
		if self.unk_0x10 != 0x20000:
			print 'error: unk_0x10: 0x{0:08X}'.format(self.unk_0x10)
			sys.exit()
		if self.unk_0x18 != 0x10:
			print 'error: unk_0x18: 0x{0:08X}'.format(self.unk_0x18)
			sys.exit()
		if self.unk_0x1C != 0x00:
			print 'error: unk_0x1C: 0x{0:08X}'.format(self.unk_0x1C)
			sys.exit()

		self.chunks = []
		for i in xrange(self.num_chunks):
			offset = f.tell()
			offset_masked = offset & 0xF
			if offset_masked <= 8:
				padding_size = struct.calcsize(Chunk.FMT) - offset_masked
			else:
				padding_size = 0x10 - offset_masked + struct.calcsize(Chunk.FMT)
			f.seek(padding_size, os.SEEK_CUR)

			chunk = Chunk()
			chunk.load(f)
			self.chunks.append(chunk)

			if i + 1 <= self.num_chunks:
				f.seek(chunk.compressed_size, os.SEEK_CUR)

with open(input_file_path, 'rb') as fi:
	if not check_file_magic(fi, BIG_MAGIC):
		print 'error: invalid file format'
		sys.exit()

	magic, num_files, unk_0x08, name_table_offset, name_table_size, max_file_name_size, max_dir_name_size, num_directories, unk_0x18, file_size = struct.unpack(HEADER_FMT, fi.read(struct.calcsize(HEADER_FMT)))

	if not unk_0x18 in [0, 1, 2]:
		print 'error: unk_0x18: 0x{0:08X}'.format(unk_0x18)
		sys.exit()

	entries = []
	for i in xrange(num_files):
		entry = Entry()
		entry.load(fi)
		entries.append(entry)

	dir_name_table_offset = name_table_offset + num_files * max_file_name_size

	fi.seek(name_table_offset)
	for i in xrange(num_files):
		dir_index, = struct.unpack('>H', fi.read(struct.calcsize('H')))

		offset = fi.tell()
		fi.seek(dir_name_table_offset + dir_index * max_dir_name_size)
		dir_name = fi.read(max_dir_name_size).partition('\0')[0]
		fi.seek(offset)

		name = fi.read(max_file_name_size - struct.calcsize('H')).partition('\0')[0]

		path = os.path.join(dir_name, name)
		entries[i].path = path

	for i in xrange(num_files):
		entry = entries[i]

		print '\t{0}'.format(entry.path)

		output_file_path = os.path.join(output_directory, entry.path)
		output_file_directory = os.path.split(output_file_path)[0]
		if len(output_file_directory) > 0:
			if not os.path.isdir(output_file_directory):
				os.makedirs(output_file_directory)

		fi.seek(entry.offset)
		data = fi.read(entry.size)

		if data.startswith(SuperChunk.MAGIC_ZIP) or data.startswith(SuperChunk.MAGIC_REFPACK):
			#with open(output_file_path + '.cmp', 'wb') as fo:
			#	fo.write(data)

			ftmp = StringIO(data)
			super_chunk = SuperChunk()
			super_chunk.load(ftmp)
			with open(output_file_path, 'wb') as fo:
				for i in xrange(super_chunk.num_chunks):
					chunk = super_chunk.chunks[i]
					ftmp.seek(chunk.offset)
					data = ftmp.read(chunk.compressed_size)
					if chunk.is_compressed:
						if super_chunk.magic == SuperChunk.MAGIC_ZIP:
							data = zlib.decompress(data, -15)
						elif super_chunk.magic == SuperChunk.MAGIC_REFPACK:
							data = refunpack(data)
						else:
							print 'error: unsupported compression type: {0}'.format(super_chunk.magic)
							sys.exit()
					fo.write(data)
		else:
			with open(output_file_path, 'wb') as fo:
				fo.write(data)
